//
//  GeneralRouter.swift
//  test001
//
//  Created by marcelo bianchi on 07/01/20.
//  Copyright © 2020 marcelo bianchi. All rights reserved.
//


import UIKit

enum GeneralRoute: IRouter {
    case home
    case main
    case detail
    case profile
    case login
    case setting(parameters: [String: Any])
}

extension GeneralRoute {
    var module: UIViewController? {
        switch self {
        case .home:
            print("home")
        case .main:
            print("main")
        case .detail:
            print("detail")
        case .profile:
            print("profile")
        case .login:
            return LoginConfiguration.setup()
            //print("login")
        case .setting(let parameters):
            print("setting")
        }
        return UIViewController()
    }
}

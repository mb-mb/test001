//
//  ViewController.swift
//  test001
//
//  Created by marcelo bianchi on 07/01/20.
//  Copyright © 2020 marcelo bianchi. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    
    let blueView: UIView = {
        let v = UIView(frame: CGRect(x: 10, y: 50, width: 50, height: 50))
        v.backgroundColor = UIColor.blue
        return v
    }()
    let greenView: UIView = {
        let v = UIView(frame: CGRect(x: 15, y: 55, width: 50, height: 50))
        v.backgroundColor = UIColor.green
        return v
    }()

    let yellowView: UIView = {
        let v = UIView(frame: CGRect(x: 20, y: 60, width: 50, height: 50))
        v.backgroundColor = UIColor.yellow
        return v
    }()
    
    let loginButton: UIButton = {
        
        let btn = UIButton(frame: CGRect(x: 150, y: 50, width: 120, height: 50))
        btn.backgroundColor = UIColor.gray
        btn.setTitle("login", for: .normal)
        btn.contentEdgeInsets = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
        btn.clipsToBounds = true
        btn.layer.cornerRadius = 8.0
        btn.addTarget(self, action: #selector(navigateToHome), for: .touchUpInside)
        
        return btn
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor(red: 57/255, green: 57/255, blue: 57/255, alpha: 1.0)
        
        showViews()
    }
    
    @objc func navigateToHome() {
        navigate(type: .modal, module: GeneralRoute.login)
    }
    
    func showViews() {
        view.addSubview(blueView)
        view.addSubview(greenView)
        view.addSubview(yellowView)
        view.addSubview(loginButton)


        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.animateViews()
        }

        DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
            self.animateViews()
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
            self.animateViews()
        }

    }

    func animateViews() {
        UIView.animate(withDuration: 1, animations: {
            self.blueView.frame = self.getPosition(v: self.blueView)
        }) { (value) in
            UIView.animate(withDuration: 0.5, animations: {
                self.blueView.frame = self.getPosition(v: self.blueView)
                self.blueView.transform =  self.blueView.transform.rotated(by: CGFloat(Double.pi / 2))
            }) { (value) in
                UIView.animate(withDuration: 1, animations: {
                    self.greenView.frame = self.getPosition(v: self.greenView)
                }) { (value) in
                    UIView.animate(withDuration: 0.5, animations: {
                        self.greenView.frame = self.getPosition(v: self.greenView)
                        self.greenView.transform =  self.greenView.transform.rotated(by: CGFloat(Double.pi / 2 ))
                    }) { (value) in
                        UIView.animate(withDuration: 1, animations: {
                                self.yellowView.frame = self.getPosition(v: self.yellowView)
                            }) { (value) in
                                UIView.animate(withDuration: 0.5, animations: {
                                    self.yellowView.frame = self.getPosition(v: self.yellowView)
                                    self.yellowView.transform =  self.yellowView.transform.rotated(by: CGFloat(Double.pi / 2 ))
                                }) {(value) in
                                    print("ok")
                                }
                        }

                    }

                }

            }
        }
    }
        
    
    func getPosition(v: UIView) -> CGRect {
        let xPosition = v.frame.origin.x
        var yPosition = v.frame.origin.y + 50 // Slide Up - 20px

        if yPosition > self.view.frame.height {
            yPosition = 50
        }
        
        let width = v.frame.size.width
        let height = v.frame.size.height
        return CGRect(x: xPosition, y: yPosition, width: width, height: height)
    }
}


//
//  OrdersMemStore.swift
//  test001
//
//  Created by marcelo bianchi on 07/01/20.
//  Copyright © 2020 marcelo bianchi. All rights reserved.
//


import Foundation

class OrdersMemStore: OrdersStoreProtocol, OrdersStoreUtilityProtocol
{
    func fetchOrders(completionHandler: @escaping ([Order], OrdersStoreError?) -> Void) {
        completionHandler(type(of: self).orders, nil)
    }
    
    func fetchOrder(id: String, completionHandler: @escaping (Order?, OrdersStoreError?) -> Void) {
        if let index = indexOfOrderWithID(id: id) {
            let order = type(of: self).orders[index]
            completionHandler(order, nil)
        } else {
            completionHandler(nil, OrdersStoreError.CannotFetch("Cannot fetch order with id \(id)"))
        }
        
    }
    
    func createOrder(orderToCreate: Order, completionHandler: @escaping (Order?, OrdersStoreError?) -> Void) {
        
    }
    
    func updateOrder(orderToUpdate: Order, completionHandler: @escaping (Order?, OrdersStoreError?) -> Void) {
        
    }
    
    func deleteOrder(id: String, completionHandler: @escaping (Order?, OrdersStoreError?) -> Void) {
        
    }
    
    func fetchOrders(completionHandler: @escaping OrdersStoreFetchOrdersCompletionHandler) {
        
    }
    
    func fetchOrder(id: String, completionHandler: @escaping OrdersStoreFetchOrderCompletionHandler) {
        
    }
    
    func createOrder(orderToCreate: Order, completionHandler: @escaping OrdersStoreCreateOrderCompletionHandler) {
        
    }
    
    func updateOrder(orderToUpdate: Order, completionHandler: @escaping OrdersStoreUpdateOrderCompletionHandler) {
        
    }
    
    func deleteOrder(id: String, completionHandler: @escaping OrdersStoreDeleteOrderCompletionHandler) {
        
    }
    
    func fetchOrders(completionHandler: @escaping (() throws -> [Order]) -> Void) {
        completionHandler { return type(of: self).orders }
    }
    
    func fetchOrder(id: String, completionHandler: @escaping (() throws -> Order?) -> Void) {
        if let index = indexOfOrderWithID(id: id) {
            completionHandler { return type(of: self).orders[index] }
        } else {
            completionHandler { throw OrdersStoreError.CannotFetch("Cannot fetch order with ID: \(id)")}
        }
    }
    
    func createOrder(orderToCreate: Order, completionHandler: @escaping (() throws -> Order?) -> Void) {
        
    }
    
    func updateOrder(orderToUpdate: Order, completionHandler: @escaping (() throws -> Order?) -> Void) {
        
    }
    
    func deleteOrder(id: String, completionHandler: @escaping (() throws -> Order?) -> Void) {
        
    }
    
    // MARK: - Convenience methods
    
    private func indexOfOrderWithID(id: String?) -> Int? {
      return type(of: self).orders.firstIndex { return $0.id == id }
    }
    
    
    // MARK: data
    static var billingAddress = Address(street1: "1 Infinite Loop", street2: "", city: "Cupertino", state: "CA", zip: "95014")
    static var shipmentAddress = Address(street1: "One Microsoft Way", street2: "", city: "Redmond", state: "WA", zip: "98052-7329")
    static var paymentMethod = PaymentMethod(creditCardNumber: "1234-123456-1234", expirationDate: Date(), cvv: "999")
    static var shipmentMethod = ShipmentMethod(speed: .OneDay)
    
    static var orders = [
      Order(firstName: "Amy", lastName: "Apple", phone: "111-111-1111", email: "amy.apple@clean-swift.com", billingAddress: billingAddress, paymentMethod: paymentMethod, shipmentAddress: shipmentAddress, shipmentMethod: shipmentMethod, id: "abc123", date: Date(), total: NSDecimalNumber(string: "1.23")),
      Order(firstName: "Bob", lastName: "Battery", phone: "222-222-2222", email: "bob.battery@clean-swift.com", billingAddress: billingAddress, paymentMethod: paymentMethod, shipmentAddress: shipmentAddress, shipmentMethod: shipmentMethod, id: "def456", date: Date(), total: NSDecimalNumber(string: "4.56"))
    ]
    
}

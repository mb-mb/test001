//
//  LoginView.swift
//  test001
//
//  Created by marcelo bianchi on 07/01/20.
//  Copyright © 2020 marcelo bianchi. All rights reserved.
//


import UIKit

final class LoginView: UIView {   // 1.
    required init?(coder aDecoder: NSCoder) {   // 2 - storyboard initializer
        super.init(coder: aDecoder)
        UIView.fromNib()   // 5.
    }
    init() {   // 3 - programmatic initializer
        super.init(frame: CGRect.zero)  // 4.
        UIView.fromNib()  // 6.
    }
    // other methods ...
}

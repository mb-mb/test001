//
//  test001
//
//  Created by marcelo bianchi on 07/01/20.
//  Copyright © 2020 marcelo bianchi. All rights reserved.
//


import Foundation
import UIKit


class LoginConfiguration {
    static func setup(parameters: [String: Any] = [:]) -> UIViewController {
        let controller = LoginViewController()
        let router = LoginRouter(view: controller)
        let presenter = LoginPresenter(view: controller)
        let interactor = LoginInteractor(presenter: presenter)
        
        controller.interactor = interactor
        controller.router = router
        interactor.parameters = parameters
        
        return controller
    }
    
}

//
//  LoginViewController.swift
//  test001
//
//  Created by marcelo bianchi on 07/01/20.
//  Copyright © 2020 marcelo bianchi. All rights reserved.
//


import UIKit

protocol ILoginViewController: class {
    var router: ILoginRouter? {get set}
    var interactor: ILoginInteractor? {get set}
    func displayFetchedOrders(viewModel: ListOrders.FetchOrders.ViewModel)
}

protocol ListOrdersDisplayLogic: class {
    func displayFetchedOrders(viewModel: ListOrders.FetchOrders.ViewModel)
}

class LoginViewController: UITableViewController, ILoginViewController, ViewControllerName,ListOrdersDisplayLogic {
    
    var interactor: ILoginInteractor?
    var router: ILoginRouter?
    var displayedOrders: [ListOrders.FetchOrders.ViewModel.DisplayedOrder] = []
    
    
    var nameOfNib: String = {
        return "LoginViewController"
    }()
    
    var defaultSession = URLSession(configuration: .default)
    var dataTask: URLSessionDataTask?

    
    var label: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "Sans", size: 16.0)
        label.text = "random number info"
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    var stackLogin: UIStackView = {
        var sv = UIStackView()
        sv.backgroundColor = UIColor.red
        sv.distribution = .equalCentering
        sv.axis = .vertical
        sv.alignment = .center
        sv.spacing = 2
        sv.translatesAutoresizingMaskIntoConstraints = false
        return sv
    }()
    
    var btn: UIButton = {
        let btn = UIButton(frame: CGRect(x: 50, y: 50, width: 150, height: 50))
        btn.backgroundColor = UIColor.red
        btn.setTitle("back to home", for: .normal)
        btn.contentEdgeInsets = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
        btn.clipsToBounds = true
        
        btn.addTarget(self, action: #selector(navigateToHome), for: .touchUpInside)
        
        return btn
    }()
    
    override func viewDidLoad() {
        super.viewWillAppear(true)
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.gray
        
        setupView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //fetchOrders()
    }

    
    @objc func navigateToHome(sender: UIButton) {
        router?.navigateToHome()
        self.dismiss(animated: true, completion: nil)
    }
    
    func setupView() {
        //view.addSubview(stackLogin)
        //stackLogin.topAnchor.constraint(equalTo: view.topAnchor, constant: 100).isActive = true
        //stackLogin.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 10).isActive = true
        //stackLogin.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20).isActive = true
        //stackLogin.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -100).isActive = true
        
        //stackLogin.addArrangedSubview(label)
        //stackLogin.addArrangedSubview(btn)
//        label.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 10.0).isActive = true
//        label.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -10).isActive = true
//        label.topAnchor.constraint(equalTo: view.topAnchor, constant: 20.0).isActive = true
        setupValues()
    }
    
    func setupValues() {
        let request = ListOrders.FetchOrders.Request()
        interactor?.fetchOrders(request: request)
    }
    
    
    func updateSearchResults(data: Data) {
        let info = String(data: data, encoding: .utf8)!
        print(info)
        self.label.text = info
    }
    
    func displayFetchedOrders(viewModel: ListOrders.FetchOrders.ViewModel) {
        displayedOrders = viewModel.displayedOrders
        tableView.reloadData()
    }

    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return displayedOrders.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let displayedOrder = displayedOrders[indexPath.row]
        var cell = tableView.dequeueReusableCell(withIdentifier: "OrderTableViewCell")
        if cell == nil {
            cell = UITableViewCell(style: .value1, reuseIdentifier: "OrderTableViewCell")
        }
        cell?.textLabel?.text = displayedOrder.date
        cell?.detailTextLabel?.text = displayedOrder.total
        return cell!
    }
}



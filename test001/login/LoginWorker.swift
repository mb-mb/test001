//
//  LoginWorker.swift
//  test001
//
//  Created by marcelo bianchi on 07/01/20.
//  Copyright © 2020 marcelo bianchi. All rights reserved.
//


import UIKit

class LoginWorker {
    var ordersStore: OrdersStoreProtocol
    
    init(ordersStore: OrdersStoreProtocol) {
      self.ordersStore = ordersStore
    }
    
    func fetchDay(completionHandler: @escaping (_ things: Data) -> Void) {}
    
    func fetchOrders(completionHandler: @escaping ([Order]) -> Void)
    {
      ordersStore.fetchOrders { (orders: () throws -> [Order]) -> Void in
        do {
          let orders = try orders()
          DispatchQueue.main.async {
            completionHandler(orders)
          }
        } catch {
          DispatchQueue.main.async {
            completionHandler([])
          }
        }
      }
    }
}

//
//  LoginInteractor.swift
//  test001
//
//  Created by marcelo bianchi on 07/01/20.
//  Copyright © 2020 marcelo bianchi. All rights reserved.
//


import UIKit



protocol ILoginInteractor: class {
    var parameters: [String: Any]? {get set}
    func fetchOrders(request: ListOrders.FetchOrders.Request)
}

protocol ListOrdersDataStore {
    var orders: [Order]? {get}
}

class LoginInteractor: ILoginInteractor, ListOrdersDataStore {
    
    var presenter: ILoginPresenter?
    var parameters: [String: Any]?
    var loginWorker = LoginWorker(ordersStore: OrdersMemStore())

    var orders: [Order]?
    
    init(presenter: ILoginPresenter) { 
        self.presenter = presenter
    }
    
    func fetchOrders(request: ListOrders.FetchOrders.Request) {
        loginWorker.fetchOrders { (orders)-> Void in
            self.orders = orders
            let response = ListOrders.FetchOrders.Response(orders: orders)
            self.presenter?.presentFetchedOrders(response: response)
        }
    }
}

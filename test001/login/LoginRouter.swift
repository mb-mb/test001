//
//  LoginRouter.swift
//  test001
//
//  Created by marcelo bianchi on 07/01/20.
//  Copyright © 2020 marcelo bianchi. All rights reserved.
//


import UIKit

protocol ILoginRouter: class {
    func navigateToHome()
}

class LoginRouter: ILoginRouter {
    weak var view: LoginViewController?
    
    init(view: LoginViewController?) {
        self.view = view
    }
    
    func navigateToHome()  {
        view?.navigate(type: .root, module: GeneralRoute.main)
    }
}
